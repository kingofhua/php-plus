1、HTTP协议，请搜索各种内容，把你所知道的任何内容列举出来
2、(重点) COOKEI与SESSION的关系，有何注意事项
3、(重点) PHP stream类型，请用你的话来描述什么是stream.并写一个stream操作类
4、(附加) 请搜索常见的php木马类型

### PHP错误异常处理



#### 5.15作业
- 作业1
	1. 尝试建立反射
	1. 尝试实例化，动态不定参数问题
	1. 扫描成员列表获取注释
	1. 对注释进行匹配

- 作业2
	- fpm精简版重做
	- 错误处理

- 作业3
	- ACL权限控制法

- 作业4
 - pcntl
  - 进程之间的关系 父子，亲缘
  - 进程状态 休眠，僵尸等
  - 进程信号量
  - pcntl fork
  - 进程间通讯如何实现

- 作业5
  fast-cgi流程图-自己画一个


#### 4.14作业
- 了解全局变量$_SERVER。
- 了解fastcgi整个执行流程，相当重要。
- 了解二进制权限，应用场景。
- 整理类反射，ReflectionClass，相当重要。
- 根据你的感觉写个fpm精简框架。


#### 3.24作业

- serialize json_encode 与 msgpack 的对比
- call callStatic invoke 来实现你们知道的设计模式
- stdclass 迭代接口
- session 锁
- http协议multipart、boundary以及响应头中的set-cookie
- cookie与session的关系，有何注意事项
- php stream类型，请用你的话来描述什么是stream，并写一个stream操作类
- fopen mode
- 匿名类


#### 3月9号，作业
- 了解魔术方法
- stdclass 迭代接口
- 错误处理。
- 匿名类
- 初始自动化IOC的实现
- php锁，php封装协议