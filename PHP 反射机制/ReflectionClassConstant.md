#### 反射类常量
```php
ReflectionClassConstant implements Reflector {
    /**** 属性 ****/
  
    /**
     * 类常量的名称
     * @desc 尝试写入时引发ReflectionException
     */
    public $name ;
    
    /**
     * 类常量所在class
     */
    public $class ;
    
    /**** 方法 ****/
    
    public __construct ( mixed $class , string $name )
    public static export ( mixed $class , string $name [, bool $return ] ) : string
    
    /**
     * 获取声明的类
     * @return ReflectionClass
     */
    public getDeclaringClass ( void ) : ReflectionClass
    
    /**
     * 获取类常量的注释
     * @return ReflectionClass
     */
    public getDocComment ( void ) : string
    
    /**
     * 获取常量的修饰符
     * @return int  返回 修饰符常量 的位掩码。
     */
    public getModifiers ( void ) : int
    
    /**
     * 类常量的名称
     * @return string
     */
    public getName ( void ) : string
    
    /**
     * 类常量的值
     * @return mixed
     */
    public getValue ( void ) : mixed
    
    /**
     * 是否是私有属性
     * @return string
     */
    public isPrivate ( void ) : bool

    /**
     * 是否是受保护的属性
     * @return string
     */
    public isProtected ( void ) : bool
    
    /**
     * 是否是公共属性
     * @return string
     */
    public isPublic ( void ) : bool
    
    /**
     * 返回 ReflectionClassConstant 对象字符串的表示形式
     * @return string
     */
    public __toString ( void ) : string
}
```