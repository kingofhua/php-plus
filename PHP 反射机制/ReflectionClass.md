#### 反射类
```php

ReflectionClass {
    /***常量****/
    
    /**
     * IS_IMPLICIT_ABSTRACT
     * @desc 指示了类是一个抽象类（abstract）， 因为它有抽象（abstract）方法。
     */
    const integer IS_IMPLICIT_ABSTRACT = 16 ;
    
    /**
     * IS_EXPLICIT_ABSTRACT
     * @desc 指示了类是一个抽象类（abstract）， 因为它已明确定义。
     */
    const integer IS_EXPLICIT_ABSTRACT = 32 ;
    /**
     * IS_FINAL
     * @desc 指示这是一个 final 类。
     * @waring 测试返回4
     */
    const integer IS_FINAL = 64 ;
    
    /***属性****/
    
    /**
     * name
     * @desc 类的名称。只读，并在尝试赋值的时候会抛出 ReflectionException
     */
    public $name ;
    
    /***方法****/
    
    /**
     * 构造方法
     * @param mixed $argument 既可以是包含类名的字符串（string）也可以是对象（object）。
     * @return 返回初始化完成后的 ReflectionClass 实例。
     * @throws \ReflectionException 如果要反射的 Class 不存在，抛出异常 ReflectionException。
     */    
    public __construct ( mixed $argument ) 
    
    /**
     * 导出一个类
     * @param mixed $argument 既可以是包含类名的字符串（string）也可以是对象（object）。
     * @param bool $return false 设为 TRUE 时返回导出结果，设为 FALSE（默认值）则忽略返回。
     * @return string 
     */  
    public static export ( mixed $argument [, bool $return = false ] ) : string
    
    /**
     * 获取定义过的一个常量
     * @param string $name 常量的名称。
     * @return mixed  常量的值。
     */      
    public getConstant ( string $name ) : mixed
    /**
     * 获取全部已定义的常量
     * @desc 获取某个类的全部已定义的常量，不管可见性如何定义。
     * @return array 全部已定义的常量。
     */
    public getConstants ( void ) : array
    
    /**
     * 获取类的构造函数
     * @return ReflectionMethod | null 一个 ReflectionMethod 对象，反射了类的构造函数，或者当类不存在构造函数时返回 NULL。
     */
    public getConstructor ( void ) : ReflectionMethod
    
    /**
     * 获取类的默认属性
     * @desc 默认属性的数组，其键是属性的名称，其值是属性的默认值或者在属性没有默认值时是 NULL。 这个函数不区分静态和非静态属性，也不考虑可见性修饰符。
     * @return array 全部已定义的默认属性的数组。
     */
    public getDefaultProperties ( void ) : array
    
    /**
     * 获取文档注释(类注释)
     * @desc 注释类型必须是 /** 换行 开头的 不然获取不到
     * @return string 如果存在则返回文档注释，否则返回 FALSE。。
     */
    public getDocComment ( void ) : string
    
    /**
     * 获取最后一行的行数
     * @desc 返回用户定义的类最后一行的行数，如果未知则返回 FALSE。
     * @return int
     */
    public getEndLine ( void ) : int
    
    /**
     * 获取类所在的扩展
     * @desc 比如 Redis扩展  Core扩展
     *       如果 new ReflectionClass(\Redis::class) 则返回  ReflectionExtension { public $name = "Redis"}
     *       如果 new ReflectionClass(stdClass::class) 则返回 ReflectionExtension { public $name = "Core"}
     *       如果 new ReflectionClass(COM::class) 则返回 ReflectionExtension { public $name = "COM"}
     *          
     * @return ReflectionExtension 类所处的扩展的 ReflectionExtension 对象的表示，如果是用户定义的类则返回 NULL。
     */
    public getExtension ( void ) : ReflectionExtension
    
    /**
     * 获取类所在的扩展名
     * @desc 比如 Redis扩展  Core扩展
     *       如果 new ReflectionClass(\Redis::class) 则返回 Redis
     *       如果 new ReflectionClass(stdClass::class) 则返回 Core
     *       如果 new ReflectionClass(COM::class) 则返回 COM
     *          
     * @return string 类所处的扩展的 名字，如果是用户定义的类则返回 NULL。
     */
    public getExtensionName ( void ) : string
    
    /**
     * 获取定义类的文件名
     * @desc 返回类所定义的文件名。如果这个类是在 PHP 核心或 PHP 扩展中定义的，则返回 FALSE。
     * @return string | false
     */
    public getFileName ( void ) : string
    
    /**
     * 获取接口（interface）名称
     * @return array 一个数值数组，接口（interface）的名称是数组的值。
     */
    public getInterfaceNames ( void ) : array
    
    /**
     * 获取接口
     * @return ReflectionClass[] 接口的关联数组，数组键是接口（interface）的名称，数组的值是 ReflectionClass 对象。
     */
    public getInterfaces ( void ) : array
    
    /**
     * 获取一个类方法的
     * @param string $name 方法名
     * @return ReflectionMethod  
     * @throws ReflectionException 没有方法抛出异常
     */
    public getMethod ( string $name ) : ReflectionMethod
    
    /**
     * 获取方法的数组
     * @param int $filter ReflectionMethod::IS_* 过滤结果为仅包含某些属性的方法。默认不过滤
     * @return ReflectionMethod[]
     */
    public getMethods ([ int $filter ] ) : array
    
    /**
     * 获取类的修饰符
     * @return int  返回 修饰符常量 的位掩码。
     */
    public getModifiers ( void ) : int
    
    /**
     * 获取类名
     * @desc 带命名空间的类名 App\Text\Base;
     * @return string
     */
    public getName ( void ) : string

    /**
     * 获取命名空间的名称
     * @return string 命名空间的名称
     */
    public getNamespaceName ( void ) : string
    
    /**
     * 获取父类
     * @desc 这个有个毛线用啊？
     * @return string 一个 ReflectionClass。
     */
    public getParentClass ( void ) : ReflectionClass
    
    /**
     * 获取一组属性
     * @param int $filter ReflectionProperty::IS_*
     * @return ReflectionProperty[]
     */
    public getProperties ([ int $filter ] ) : array
    
    /**
     * 获取类的一个属性
     * @param string $name 属性名。
     * @return ReflectionProperty
     * @throws ReflectionException 
     */
    public getProperty ( string $name ) : ReflectionProperty
    
    /**
     * 获取类的一个常量
     * @desc 此方法可以返回一个ReflectionClassConstant对象，可以获取注释等信息
     * @return ReflectionClassConstant | null
     */
    public getReflectionConstant ( string $name ) : ReflectionClassConstant
    
    /**
     * 获取类的所有常量
     * @desc 此方法可以返回一个ReflectionClassConstant[]对象，可以获取注释等信息
     * @return ReflectionClassConstant[]
     */
    public getReflectionConstants ( void ) : array
    
    /**
     * 获取短名 
     * @desc 获取类的短名，就是不含命名空间（namespace）的那一部分。 例如App\Text\Base 返回 Base
     */
    public getShortName ( void ) : string
    
    /**
     * 获取当前类定义 在文件开始的行数
     * @return int 行号
     */
    public getStartLine ( void ) : int
    
    /**
     * 获取当前类定义的静态属性
     * @return array 
     */
    public getStaticProperties ( void ) : array
    
    /**
     * 获取当前类定义的静态属性
     * @param string $name  静态属性名称
     * @param mixed $def_value  没有的话默认返回值
     * @return mixed 
     */
    public getStaticPropertyValue ( string $name [, mixed $def_value ] ) : mixed
    
    /**
     * 返回 trait 别名的一个数组
     * @desc use trait的时候，如果trait里面的方法和类里面的冲突 就会需要别名，这个方法是获取别名后的数组
     * @return array
     */
    public getTraitAliases ( void ) : array
    
    /**
     * 返回 当前类引用 的所有trait数组
     * @return array
     */
    public getTraitNames ( void ) : array
    
    /**
     * 返回 当前类引用 的所有trait数组
     * @return ReflectionClass[]
     */    
    public getTraits ( void ) : array
    
    /**
     * 是否有指定的常量
     * @param string $name
     * @return bool
     */
    public hasConstant ( string $name ) : bool

    /**
     * 是否有指定的方法
     * @param string $name
     * @return bool
     */
    public hasMethod ( string $name ) : bool
    
    /**
     * 是否有指定的属性
     * @param string $name
     * @return bool
     */
    public hasProperty ( string $name ) : bool

    /**
     * 检查它是否实现了一个接口
     * @param string $interfac
     * @return bool
     */
    public implementsInterface ( string $interface ) : bool

    /**
     * 检查是否位于命名空间中
     * @return bool
     */
    public inNamespace ( void ) : bool
    
    /**
     * 检查类是否是抽象类
     * @return bool
     */
    public isAbstract ( void ) : bool
    
    /**
     * 检查类是否是匿名类
     * @return bool
     */
    public isAnonymous ( void ) : bool
    
    /**
     * 返回了一个类是否可复制
     * @return bool
     */
    public isCloneable ( void ) : bool
    
    /**
     * 检查类是否声明为 final
     * @return bool
     */
    public isFinal ( void ) : bool
    
    /**
     * 检查对象是否为一个类的实例。
     * @param object $object
     * @return bool
     */
    public isInstance ( object $object ) : bool
    
    /**
     * 检查类是否可实例化。
     * @return bool
     */
    public isInstantiable ( void ) : bool
    
    /**
     * 检查类是否是一个接口。
     * @return bool
     */
    public isInterface ( void ) : bool
    
    /**
     * 检查类是否由扩展或核心在内部定义。
     * @desc 可以判断是否是用户定义的类 这个下面有个 isUserDefined 呢，僵硬
     * @return bool
     */
    public isInternal ( void ) : bool
    
    /**
     * 检查类是否是一个迭代对象。
     * @return bool
     */
    public isIterable ( void ) : bool
    
    /**
     * 检查类是否是一个可迭代的。
     * @return bool
     */
    public isIterateable ( void ) : bool
    
    /**
     * 检查是否为一个子类
     * @desc 检查一个类是否为指定类的子类，或者实现了指定的接口。
     * @param string $class
     * @return bool
     */
    public isSubclassOf ( string $class ) : bool
    
    /**
     * 返回了是否为一个 trait。
     * @return bool
     */
    public isTrait ( void ) : bool
    
    /**
     * 检查类是否是用户定义的类
     * @return bool
     */
    public isUserDefined ( void ) : bool
    
    /**
     * 从指定的参数创建一个新的类实例
     * @desc 
     *       1、如果类的构造函数不是 public 的将会导致一个 ReflectionException。
     *       2、当 args 指定了一个或多个参数，而类不具有构造函数时,将导致一个 ReflectionException。
     * @param mixed | null $args 参数
     * @throws ArgumentCountError 参数数量有问题
     * @throws ReflectionException 
     * @return object
     */
    public newInstance ( mixed $args [, mixed $... ] ) : object
    
    /**
     * 从指定的参数创建一个新的类实例
     * @desc 
     *       1、如果类的构造函数不是 public 的将会导致一个 ReflectionException。
     *       2、当 args 指定了一个或多个参数，而类不具有构造函数时,将导致一个 ReflectionException。
     * @waring 和上面一样啊，但是不知道有啥区别呢
     * @param mixed $args 参数
     * @throws ArgumentCountError 参数数量有问题
     * @throws ReflectionException 
     * @return object
     */
    public newInstanceArgs ([ array $args ] ) : object
    
    /**
     * 创建一个新的类实例而不调用它的构造函数
     * @desc 
     *       如果这个类是一个不能不调用构造函数来实例化的内置类，将导致一个 ReflectionException。
     *    在 PHP 5.6.0 及更高版本中，此异常仅限于 final 的内置类。
     * @throws ReflectionException 
     * @return object
     */
    public newInstanceWithoutConstructor ( void ) : object
    
    /**
     * 设置静态属性的值
     * @desc 这个会对 原对象的静态属性改变！
     * @return void
     */
    public setStaticPropertyValue ( string $name , string $value ) : void
    
    /**
     * 返回 ReflectionClass 对象字符串的表示形式
     * @return string
     */
    public __toString ( void ) : string
}
```
