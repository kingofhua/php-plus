#### 反射方法
```php

ReflectionMethod extends ReflectionFunctionAbstract implements Reflector {
    /**** 常量 ****/
    
    const integer IS_STATIC = 1 ;
    const integer IS_PUBLIC = 256 ;
    const integer IS_PROTECTED = 512 ;
    const integer IS_PRIVATE = 1024 ;
    const integer IS_ABSTRACT = 2 ;
    const integer IS_FINAL = 4 ;
    
    /**** 属性 ****/
    
    /**
     * 方法的名称
     */
    public $name ;
    
    /**
     * 方法所在class
     */
    public $class ;
    
    /**** 方法 ****/
    
    /**
     * 构造方法
     * @param mixed $argument 既可以是包含类名的字符串（string）也可以是对象（object）。
     * @return 返回初始化完成后的 ReflectionMethod 实例。
     * @throws \ReflectionException 如果指定的方法不存在，抛出异常 ReflectionException。
     */   
    public __construct ( mixed $class , string $name )
    
    /**
     * 返回一个动态建立的方法调用接口
     * @desc 可以使用这个返回值直接调用非公开方法()比如私有方法
     * @param object $object 只能是当前类或者是继承、实现了该类的
     * @return Closure 返回 Closure 如果产生任何错误返回 NULL
     */
    public getClosure ( object $object ) : Closure
    
    /**
     * 获取声明的类
     * @return ReflectionClass
     */
    public getDeclaringClass ( void ) : ReflectionClass
    
    /**
     * 获取方法的修饰符
     * @return int  返回 修饰符常量 的位掩码。
     */
    public getModifiers ( void ) : int
    
    /**
     * 获取方法的原型
     * @desc 多态中查找最开始定义的方法
     * @return ReflectionMethod  返回 ReflectionMethod (方法在最开始定义的类)
     * @throws ReflectionException  如果方法没有原型，产生一个 ReflectionException
     */
    public getPrototype ( void ) : ReflectionMethod
    
    /**
     * 执行一个反射的方法。
     * @param object $object 如果执行的方法是静态类，那么这个参数传送 null。
     * @param mixed $parameter 0，或者传送给方法的参数列表。可以通过这个参数，给方法传送大量的参数。
     * @return mixed 返回方法的返回值
     * @throws  ReflectionException 如果 object 并没有包含一个可以使用的类实例，那么将产生 一个 ReflectionException。如果方法调用失败，也会产生一个 ReflectionException。
     */
    public invoke ( object $object [, mixed $parameter [, mixed $... ]] ) : mixed

    /**
     * 执行一个反射的方法。
     * @param object $object 如果执行的方法是静态类，那么这个参数传送 null。
     * @param array $args  使用 array 传送的方法参数。
     * @return mixed 返回方法的返回值
     * @throws  ReflectionException 如果 object 并没有包含一个可以使用的类实例，那么将产生 一个 ReflectionException。如果方法调用失败，也会产生一个 ReflectionException。
     */
    public invokeArgs ( object $object , array $args ) : mixed
    
    /**
     * 判断方法是否是抽象方法
     * @return bool
     */
    public isAbstract ( void ) : bool

    /**
     * 判断方法是否是构造方法
     * @return bool
     */
    public isConstructor ( void ) : bool
    
    /**
     * 判断方法是否是析构方法
     * @return bool
     */
    public isDestructor ( void ) : bool
    
    /**
     * 判断方法是否定义 final
     * @return bool
     */
    public isFinal ( void ) : bool
    
    /**
     * 判断方法是否是私有方法
     * @return bool
     */
    public isPrivate ( void ) : bool
    
    /**
     * 判断方法是否是保护方法
     * @return bool
     */
    public isProtected ( void ) : bool
    
    /**
     * 判断方法是否是公开方法
     * @return bool
     */
    public isPublic ( void ) : bool

    /**
     * 判断方法是否是静态方法
     * @return bool
     */
    public isStatic ( void ) : bool
    
    /**
     * 设置方法是否访问
     * @param bool $accessible
     */
    public setAccessible ( bool $accessible ) : void
    
    
    public __toString ( void ) : string
}
```