<?php

require_once __DIR__ . '/BaseTrait.php';
require_once __DIR__ . '/Base.php';

use App\Text;

class King extends Text\Base
{
    public function getName()
    {
        echo __CLASS__;
    }
}

function kingofzihua($name = 'wang', $age = '23')
{
    echo $name . ':' . $age;
    return $name . ':' . $age;
}

$class = new Text\Base();

$classMethod = new ReflectionMethod(King::class, 'getName');

$ClosureScopeClass = $classMethod->getClosureScopeClass();
$ClosureThis = $classMethod->getClosureThis();
$EndLine = $classMethod->getEndLine();


function aa($aa)
{
    return $aa();
}

// Text\base_func();
$extension = (new ReflectionFunction('get_parent_class'))->returnsReference();

var_dump($extension);