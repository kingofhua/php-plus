<?php
require_once __DIR__ . '/BaseTrait.php';
require_once __DIR__ . '/Base.php';

use App\Text;

//__construct

/**
 * @var $reflectionClass => ReflectionClass Object
 * (
 *      [name] => Base
 * )
 */
$reflectionClass = new ReflectionClass(Text\Base::class);

/**
 * Class [ <user> class Base ] {
 * @@ /Applications/XAMPP/xamppfiles/htdocs/phpplus/PHP 反射机制/code/test/Base.php 4-27
 *
 * - Constants [1] {
 * Constant [ public string NAME ] { kingofzihua }
 * }
 *
 * - Static properties [0] {
 * }
 *
 * - Static methods [0] {
 * }
 *
 * - Properties [3] {
 * Property [ <default> public $public_params ]
 * Property [ <default> private $private_params ]
 * Property [ <default> protected $protected_params ]
 * }
 *
 * - Methods [3] {
 * Method [ <user> public method public_function ] {
 * @@ /Applications/XAMPP/xamppfiles/htdocs/phpplus/PHP 反射机制/code/test/Base.php 13 - 16
 * }
 *
 * Method [ <user> private method private_function ] {
 * @@ /Applications/XAMPP/xamppfiles/htdocs/phpplus/PHP 反射机制/code/test/Base.php 18 - 21
 * }
 *
 * Method [ <user> protected method protected_function ] {
 * @@ /Applications/XAMPP/xamppfiles/htdocs/phpplus/PHP 反射机制/code/test/Base.php 23 - 26
 * }
 * }
 * }
 */
$export = $reflectionClass::export(Text\Base::class, true);

/**
 * @var mixed $name_constant kingofzihua
 */
$name_constant = $reflectionClass->getConstant('NAME');

/**
 * @var array $constants
 */
$constants = $reflectionClass->getConstants();

/**
 * @var ReflectionMethod | null $constructor =>ReflectionMethod Object
 * (
 * [name] => __construct
 * [class] => Base
 * )
 */
$constructor = $reflectionClass->getConstructor();

/**
 * @var array $defaultProperties =>Array
 * (
 *    [public_params] => default_public_params
 *    [private_params] => default_private_params
 *    [protected_params] => default_protected_params
 * )
 */
$defaultProperties = $reflectionClass->getDefaultProperties();

/**
 * @var string $docComment => /**
 * *\/
 */
$docComment = $reflectionClass->getDocComment();

/**
 * @var int $endLine 35
 */
$endLine = $reflectionClass->getEndLine();

/**
 * @var ReflectionExtension |null $extension
 */
$extension = $reflectionClass->getExtension();

/**
 * @var string | null $extensionName
 */
$extensionName = $reflectionClass->getExtensionName();

/**
 * @var string|false $fileName /Applications/XAMPP/xamppfiles/htdocs/phpplus/PHP 反射机制/code/test/Base.php
 */
$fileName = $reflectionClass->getFileName();

/**
 * @var array $interfaceNames
 */
$interfaceNames = $reflectionClass->getInterfaceNames();

/**
 * @var ReflectionClass[] $interfaces => Array
 * (
 *      [Serializable] => ReflectionClass Object
 *          (
 *              [name] => Serializable
 *          )
 * )
 */
$interfaces = $reflectionClass->getInterfaces();


/**
 * @var ReflectionMethod $method => ReflectionMethod Object
 * (
 *      [name] => name
 *      [class] => Base
 * )
 * @throws ReflectionException
 */
$name_method = $reflectionClass->getMethod('name');

/**
 * @var ReflectionMethod[] $methods =>Array
 * (
 *      [0] => ReflectionMethod Object
 *              (
 *                  [name] => __construct
 *                  [class] => Base
 *              )
 *
 *      [1] => ReflectionMethod Object
 *              (
 *                  [name] => name
 *                  [class] => Base
 *              )
 *      ...
 * )
 */
$methods = $reflectionClass->getMethods();

/**
 * @var int $modifiers 524288
 */
$modifiers = $reflectionClass->getModifiers();

/**
 * @var string $className Base
 */
$className = $reflectionClass->getName();

/**
 * @var string $namespaceName
 */
$namespaceName = $reflectionClass->getNamespaceName();

/**
 * @var string $parentClass
 */
$parentClass = $reflectionClass->getParentClass();

/**
 * @var ReflectionProperty[] $properties
 */
$properties = $reflectionClass->getProperties();

/**
 * @var ReflectionProperty $property
 * @throws ReflectionException:
 */
$nameProperty = $reflectionClass->getProperty('name');

/**
 * @var ReflectionClassConstant $nameReflectionConstant
 */
$nameReflectionConstant = $reflectionClass->getReflectionConstant('NAME');

/**
 * @var ReflectionClassConstant[] $reflectionConstants
 */
$reflectionConstants = $reflectionClass->getReflectionConstants();

/**
 * @var string $shortName
 */
$shortName = $reflectionClass->getShortName();

/**
 * @var int $startLine
 */
$startLine = $reflectionClass->getStartLine();

/**
 * @var array $staticProperties
 */
$staticProperties = $reflectionClass->getStaticProperties();

/**
 * @var mixed $staticProperty
 */
$staticProperty = $reflectionClass->getStaticPropertyValue('statusMap', ['success' => 1, 'error' => 0]);

/**
 * @var array $traitAliases
 */
$traitAliases = $reflectionClass->getTraitAliases();

/**
 * @var array $traitNames
 */
$traitNames = $reflectionClass->getTraitNames();

/**
 * @var ReflectionClass[] $traits
 */
$traits = $reflectionClass->getTraits();

/**
 * @var bool $hasNameConstant
 */
$hasNameConstant = $reflectionClass->hasConstant('NAME');

/**
 * @var bool $inNamespace
 */
$inNamespace = $reflectionClass->inNamespace();

/**
 * @var bool $isInternal
 */
$isInternal = $reflectionClass->isInternal();

/**
 * @var object $newClass
 * @throws ArgumentCountError
 * @throws  ReflectionException
 */
// $newClass = $reflectionClass->newInstance(['a'=>'123']);

// $newClass = $reflectionClass->newInstanceArgs(['a'=>'123']);
$newClass = $reflectionClass->newInstanceWithoutConstructor();

$reflectionClass->setStaticPropertyValue('statusMap', [0, 1]);

$reflectionClass->__toString();

$modifierNames = Reflection::getModifierNames($modifiers);

print_r($modifierNames);