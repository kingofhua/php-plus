<?php

require_once __DIR__ . '/BaseTrait.php';
require_once __DIR__ . '/Base.php';

use App\Text;


$reflectionClass = new ReflectionClass(Text\Base::class);

/**
 * @var ReflectionClassConstant $nameReflectionConstant
 */
$nameReflectionConstant = $reflectionClass->getReflectionConstant('NAME');

/**
 * @var ReflectionClass $declaringClass
 */
$declaringClass = $nameReflectionConstant->getDeclaringClass();

/**
 * @var string $docComment
 */
$docComment = $nameReflectionConstant->getDocComment();

/**
 * @var int $docComment
 */
$modifiers = $nameReflectionConstant->getModifiers();

/**
 * @var string $name
 */
$name = $nameReflectionConstant->getName();

/**
 * @var mixed $value
 */
$value = $nameReflectionConstant->getValue();

$modifierNames = Reflection::getModifierNames($modifiers);

print_r($modifierNames);