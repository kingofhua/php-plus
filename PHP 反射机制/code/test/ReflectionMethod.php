<?php

require_once __DIR__ . '/BaseTrait.php';
require_once __DIR__ . '/Base.php';

use App\Text;

class King extends Text\Base
{
    public function getName()
    {
        echo __CLASS__;
    }
}

class Kingofizhua extends King
{
    public function getName()
    {
        echo __CLASS__;
    }
}

$class = new Text\Base();

// Create an instance of the ReflectionMethod class
$method = new ReflectionMethod(Kingofizhua::class, 'name');

/**
 * @var Closure $closure
 */
$closure = $method->getClosure(new Kingofizhua());

/**
 * @var ReflectionMethod $prototype
 * @throws ReflectionException
 */
// $prototype = $method->getPrototype();
if (!$method->isPublic()) {
    $method->setAccessible(true);
}

$invoke = $method->invoke(new Kingofizhua());

print_r($prototype);