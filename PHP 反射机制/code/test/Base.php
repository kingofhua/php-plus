<?php

namespace App\Text;


/**
 * Class Base
 * @auth: kingofzihua
 * @date: 2019-06-18
 */
class Base implements \Serializable
{
    use BaseTrait {
        BaseTrait::getName as getNameTrait;
    }

    /**
     * 名称
     */
    const NAME = 'kingofzihua';
    const STATUS_SUCCESS = 'success';
    const STATUS_ERROR = 'error';

    public static $statusMap = [
        self::STATUS_SUCCESS => 'success',
        self::STATUS_ERROR => 'error',
    ];

    public $name;

    public $public_params = 'default_public_params';

    private $private_params = 'default_private_params';

    protected $protected_params = 'default_protected_params';

    public function __construct()
    {
    }

    public function getName()
    {
        return $this->name();
    }

    protected function name()
    {
        echo 'kingofzihua';
    }

    public function public_function()
    {
        echo __CLASS__ . ':' . __METHOD__;
    }

    private function private_function()
    {
        echo __CLASS__ . ':' . __METHOD__;
    }

    protected function protected_function()
    {
        echo __CLASS__ . ':' . __METHOD__;
    }

    public function serialize()
    {
        return json_encode($this);
    }

    public function unserialize($serialized)
    {
        return json_decode($serialized);
    }
}

/**
 * @deprecated
 * @auth: kingofzihua
 * @param int $a
 * @param null $b
 * @return int
 *
 */
function base_func(int $a = 1, $b = null): int
{
    static $NAME = '123';

    return $a;
}