<?php

namespace App\Annotations;

/**
 *
 * @Annotation
 */
class Inject
{
    /**
     * @auth: kingofzihua
     * @var string
     */
    protected $className;

    public function __construct($className)
    {
        $this->className = $className['value'];
    }

    public function getClass()
    {
        return new $this->className;
    }
}