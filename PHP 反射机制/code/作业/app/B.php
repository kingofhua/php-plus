<?php

namespace App;

use App\Annotations\Inject;

class B
{
    /**
     * @inject(A::class)
     */
    public $a;

    public function __construct()
    {

    }
}