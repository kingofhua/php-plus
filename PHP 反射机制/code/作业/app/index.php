<?php
require_once __DIR__ . '/../vendor/autoload.php';

require_once __DIR__ . '/A.php';
require_once __DIR__ . '/B.php';
require_once __DIR__ . '/ClassScanner.php';
require_once __DIR__ . '/Annotations/Inject.php';

use  App\A;
use  App\B;
use  App\ClassScanner;
use  App\Annotations\Inject;
use Doctrine\Common\Annotations\AnnotationReader;

/**
 * 1、通过反射获取类
 * 2、获取所有的属性
 * 3、便利所有属性 并获取注释
 * 4、处理所有的 inject
 * 5、获取所对应的类
 * 6、实力化类并且付给变量a
 * 7、返回所实力化的类
 */
$classB = new ClassScanner(B::class);

$classB->autoInject()->a->say();