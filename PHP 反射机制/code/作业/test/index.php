<?php
require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/Foo.php';
require_once __DIR__ . '/MyAnnotation.php';

use Doctrine\Common\Annotations\AnnotationReader;

$reflectionClass = new \ReflectionClass(Foo::class);
$property = $reflectionClass->getProperty('bar');

$reader = new AnnotationReader();
$myAnnotation = $reader->getPropertyAnnotation(
    $property,
    MyAnnotation::class
);

echo $myAnnotation->myProperty; // result: "value"
