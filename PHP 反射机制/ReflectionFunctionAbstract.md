```php
ReflectionFunctionAbstract implements Reflector {
    /* 属性 */
    public $name ;
    /* 方法 */
    
    /**
     * 复制函数
     * @desc 不允许复制
     */
    final private __clone ( void ) : void
    
    /**
     * 返回与闭包关联的作用域
     * @desc 不知道啥东西!!!!
     */
    public getClosureScopeClass ( void ) : ReflectionClass
    
    /**
     * 返回本身的匿名函数
     * @desc 不知道啥东西!!!!
     * @return 返回 $this 指向，产生错误返回 NULL
     */
    public getClosureThis ( void ) : object
    
    /**
     * 获取注释
     * @return ReflectionClass
     */
    public getDocComment ( void ) : string
    
    /**
     * 获取最后一行的行数
     * @desc 返回用户定义的方法最后一行的行数，如果未知则返回 FALSE。
     * @return int
     */
    public getEndLine ( void ) : int
    
    /**
     * 获取类所在的扩展
     * @desc 比如 Core扩展
     *       如果 new ReflectionFunction('get_parent_class') 则返回  ReflectionExtension { [name] => Core }
     *          
     * @return ReflectionExtension 类所处的扩展的 ReflectionExtension 对象的表示，如果是用户定义的类则返回 NULL。
     */
    public getExtension ( void ) : ReflectionExtension
    
    /**
     * 获取类所在的扩展名
     * @desc 比如 Core扩展
     *       如果 new ReflectionFunction('get_parent_class') 则返回  Core
     *          
     * @return string 类所处的扩展的 名字，如果是用户定义的类则返回 NULL。
     */
    public getExtensionName ( void ) : string
    
    /**
     * 获取定义函数的文件名
     * @desc 返回类所定义的文件名。如果这个类是在 PHP 核心或 PHP 扩展中定义的，则返回 FALSE。
     * @return string | false
     */
    public getFileName ( void ) : string
    
    /**
     * 获取函数名
     * @desc 带命名空间的类名 App\Text\Base_func;
     * @return string
     */
    public getName ( void ) : string
    
    /**
     * 获取命名空间的名称
     * @return string 命名空间的名称
     */
    public getNamespaceName ( void ) : string
    
    /**
     * 获取函数参数数量
     * @return int 参数数量
     */
    public getNumberOfParameters ( void ) : int
    
    /**
     * 获取函数必填参数数量
     * @return int 参数数量
     */
    public getNumberOfRequiredParameters ( void ) : int
   
    /**
     * 获取函数参数列表
     * @return ReflectionParameter[] 参数列表
     */
    public getParameters ( void ) : array
    
    /**
     * 获取函数返回值类型
     * @return ReflectionNamedType| null 参数列表
     */
    public getReturnType ( void ) : ReflectionType
    
    /**
     * 获取短名 
     * @desc 获取方法的短名，就是不含命名空间（namespace）的那一部分。 例如App\Text\Base_func 返回 Base_func
     */
    public getShortName ( void ) : string
    
    /**
     * 获取当前方法定义 在文件开始的行数
     * @return int 行号
     */
    public getStartLine ( void ) : int
    
    /**
     * 获取当前方法内定义的静态变量
     * @return array 变量数组
     */
    public getStaticVariables ( void ) : array
    
    /**
     * 检查函数是否指定返回类型
     * @return bool
     */
    public hasReturnType ( void ) : bool
    
    /**
     * 检查函数是否有命名空间
     * @return bool
     */
    public inNamespace ( void ) : bool
    
    /**
     * 检查是否是匿名函数
     * @return bool
     */
    public isClosure ( void ) : bool
    
    /**
     * 检查是否已经弃用
     */
    public isDeprecated ( void ) : bool
    
    /**
     * 判断函数是否是一个生成器函数
     */
    public isGenerator ( void ) : bool
    
    /**
     * 判断函数是否是内置函数
     */
    public isInternal ( void ) : bool
    
    /**
     * 检查是否是用户定义
     */
    public isUserDefined ( void ) : bool
    
    /**
     * 是否是可变函数
     */
    public isVariadic ( void ) : bool
    
    /**
     * 检查是否返回参考信息
     * @desc 参考信息
     */
    public returnsReference ( void ) : bool
    abstract public __toString ( void ) : void
}
```